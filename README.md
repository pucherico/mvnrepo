# pucherico maven repository

Welcome to my public maven repository.

In your ```pom.xml```, add this snippet:
  
```
<repositories>
    <repository>
        <id>pucherico-release</id>
        <name>pucherico maven repository</name>
        <url>https://bitbucket.org/pucherico/mvnrepo/raw/master/release/</url>
        <layout>default</layout>
    </repository>
</repositories>
```